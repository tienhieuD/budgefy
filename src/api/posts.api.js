import { POSTS_ENDPOINT, POSTS_LIMIT } from './config';

export const getPosts = async ({ page, limit }) => {
    const url = `${POSTS_ENDPOINT}?page=${page || 1}&limit=${limit || POSTS_LIMIT}`
    const response = await fetch(url)
    const posts = await response.json()
    return posts
}

export const getPostById = async (id) => {
    const url = `${POSTS_ENDPOINT}/${id}`
    const response = await fetch(url)
    const posts = await response.json()
    return posts
}
