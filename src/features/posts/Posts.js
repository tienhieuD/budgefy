import _ from 'lodash';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { POSTS_LIMIT } from '../../api/config';
import { fetchPostById, fetchPosts } from './postsSlice';

export default function Posts({ match }) {
    const dispatch = useDispatch()
    const { posts, status } = useSelector(state => state.posts)
    const { pageNumber } = match.params

    React.useEffect(() => {
        dispatch(fetchPosts({ limit: POSTS_LIMIT, page: pageNumber }))
    }, [pageNumber])

    const postsList = (
        <>
            <ul class="list-posts">
                {posts.map(post => <PostItem key={post.id} {...post} />)}
            </ul>
            <Pager />
        </>
    )

    return (status === 'pending' ? <Loading /> : postsList)
}

function Loading() {
    return <span>Loading...</span>
}

function Pager() {
    const { currentPage, totalPage } = useSelector(state => state.posts)
    const previousPage = currentPage - 1;
    const nextPage = currentPage + 1;
    const handleClick = () => {
        const html = document.getElementsByTagName('html')
        if (html.length)
            html[0].scrollTop = 0
    }

    const PageLink = ({ page, label, disabled }) => (
        <span>
            {disabled ? label : <Link to={`/page/${page}`} onClick={handleClick}>
                {label}
            </Link>}
        </span>
    )

    return <div class="pager">
        {<PageLink page={previousPage} label="<Prev" disabled={previousPage < 1} />}
        {_.range(1, totalPage + 1).map(page => <PageLink key={page} page={page} label={page} disabled={page === currentPage} />)}
        {<PageLink page={nextPage} label="Next>" disabled={nextPage > totalPage} />}
    </div>
}

function PostItem({ id, publish_date, name, tags }) {
    return (
        <li class="blog-item">
            <span class="date-publish mute">{publish_date}</span>
            <span class="tags">{tags}</span>
            <Link to={`/post/${id}`} class="blog-name">
                {name}
            </Link>
        </li>
    )
}

export function PostDetail({ match }) {
    const dispatch = useDispatch()
    const { currentPost, currentPostStatus } = useSelector(state => state.posts)
    const postId = match.params?.postId
    const content = currentPost && currentPostStatus !== 'pending' && currentPost.content || ''


    React.useEffect(() => {
        dispatch(fetchPostById(postId))
    }, [postId]);

    return (content ? <ReactMarkdown source={content} escapeHtml={true} /> : <Loading />)
}