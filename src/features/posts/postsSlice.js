import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { getPostById, getPosts } from '../../api/posts.api';

export const fetchPosts = createAsyncThunk('posts/fetchPosts', ({ page, limit }) => {
    return getPosts({ page, limit })
})

export const fetchPostById = createAsyncThunk('posts/fetchPostById', (id) => {
    return getPostById(id)
})

const postsSlice = createSlice({
    name: 'posts',
    initialState: {
        currentPage: 1,
        totalPage: 5,
        posts: [],
        status: 'idle',
        error: null,
        currentPost: null,
        currentPostStatus: 'idle',
        currentPostError: null,
    },
    reducers: {
        setCurrentPage: (state, action) => {
            state.currentPage = action.payload
        }
    },
    extraReducers: {
        [fetchPosts.pending]: (state, action) => {
            state.status = 'pending'
        },
        [fetchPosts.fulfilled]: (state, action) => {
            state.status = 'fulfilled'
            state.posts = action.payload
            if (action.meta.arg?.page) {
                state.currentPage = Number(action.meta.arg?.page)
            }
        },
        [fetchPosts.rejected]: (state, action) => {
            state.status = 'rejected'
            state.error = action.payload
        },

        [fetchPostById.pending]: (state, action) => {
            state.currentPostStatus = 'pending'
        },
        [fetchPostById.fulfilled]: (state, action) => {
            state.currentPostStatus = 'fulfilled'
            state.currentPost = action.payload
        },
        [fetchPostById.rejected]: (state, action) => {
            state.currentPostStatus = 'rejected'
            state.currentPostError = action.payload
        },
    }
})

export const { setCurrentPage } = postsSlice.actions

export default postsSlice.reducer
