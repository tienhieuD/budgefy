import React from 'react'
import logo from '../assets/img/logo.svg'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";

export default function Header() {
    return (
        <header>
            <div class="logo-web-name">
                <div class="logo">
                    <Link to="/">
                        <img src={logo} alt="logo" />
                    </Link>
                </div>
                <h1 class="web-name"> Henlo, fren! </h1>
            </div>
            <p class="slogan mute quote">from tienhieuD</p>
            <HeaderNav />
        </header>
    )
}

function HeaderNav() {
    return (
        <nav class="menu">
            <ul>
                <li><Link to="/about">About</Link></li>
                <li><Link to="/rss.xml">RSS</Link></li>
            </ul>
        </nav>
    )
}