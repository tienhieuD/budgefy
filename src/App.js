import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Footer from './app/Footer';
import Header from './app/Header';
import Posts, { PostDetail } from './features/posts/Posts';

function App() {
  return (<>
    <Router>
      <Header />
      <main class="container">
        <Route exact path="/" component={Posts} />
        <Route exact path="/page/:pageNumber" component={Posts} />
        <Route exact path="/post/:postId" component={PostDetail} />
        <Route exact path="/about" component={PostDetail} />
      </main>
      <Footer />
    </Router>
  </>)
}

export default App
